const {User} = require('../models')

function format(user){
    const {id, username} = user

    return {
        id,
        username,
        accessToken : user.generateToken()
    }
}

module.exports = {
    login: async (req,res) => {
        try{
            const user = User.authenticate(req.body)
            res.json(format(user))
        }catch(e){
            res.json(403, {
                message: 'Login Failed'
            })
        }
    }
}
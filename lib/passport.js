const passport = require('passport')
const {Strategy: jwtStrategy, ExtractJwt} = require('passport-jwt')
const JwtStrategy = require('passport-jwt/lib/strategy')
const {User} = require('../models')
const options = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'ini rahasia'
}

passport.use(
    JwtStrategy(options, async (payload, done) => {
        try{
            const user = await User.findByPk(payload.id)
            done(null, user)
        }catch(e){
            done(err, false)
        }
    })
)

module.exports = passport